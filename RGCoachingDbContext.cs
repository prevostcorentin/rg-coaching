﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RGCoaching.Models;

namespace RGCoaching
{
    public class RGCoachingDbContext : IdentityDbContext

    {
        public virtual DbSet<Models.Page> Pages { get; set; }
        public virtual DbSet<Models.MenuLink> Links { get; set; }
        public virtual DbSet<Models.Customer> Customers { get; set; }
        public virtual DbSet<Models.Block> Blocks { get; set; }
        public virtual DbSet<Models.Contact> Contacts { get; set; }
        public virtual DbSet<Models.Program> Programs { get; set; }
        public virtual DbSet<Models.PageBlock> PageBlocks { get; set; }

        public RGCoachingDbContext(DbContextOptions<RGCoachingDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<PageBlock>()
                   .HasOne(blockPage => blockPage.Page)
                   .WithMany(page => page.PageBlocks)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<PageBlock>()
                   .HasOne(pageBlock => pageBlock.Block)
                   .WithMany(block => block.PageBlocks);
                   

            builder.Entity<Customer>()
                   .HasMany(customer => customer.Programs)
                   .WithOne(program => program.Customer)
                   .OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(builder);
        }

    }
  
}
