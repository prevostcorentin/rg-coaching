﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RGCoaching.Migrations
{
    public partial class Gender : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Gender",
                table: "Customers",
                nullable: true);

            migrationBuilder.Sql(@"
                INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'22781f20-67cb-4e0e-baab-d9c5bc97893d', N'Administrator', N'ADMINISTRATOR', N'bd3458db-c81e-45bc-aaea-dfe66f9b4e46')
                INSERT[dbo].[AspNetRoles]([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES(N'7565d219-0356-4c1b-ba17-c450a9021d67', N'User', N'USER', N'0b53746b-fa80-4097-88f1-92cddbe068db')
                GO
                INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'b58846ed-49b6-4edd-a857-be45753a71c3', N'pierre.dupin@email.com', N'PIERRE.DUPIN@EMAIL.COM', N'pierre.dupin@email.com', N'PIERRE.DUPIN@EMAIL.COM', 1, N'AQAAAAEAACcQAAAAEMLaMl5vBU++b7OOhosveaKAxoJBHMMRXDeM8/SYhUbrxaz5qMtg+MQsrCW4M5Eifg==', N'5UPOS3WEQ5GQG6Z3P2SB2TOLSKJH2AEN', N'c26e0635-e1b0-4800-9831-30034fb86734', NULL, 0, 0, NULL, 1, 0)
                GO
                INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b58846ed-49b6-4edd-a857-be45753a71c3', N'7565d219-0356-4c1b-ba17-c450a9021d67')
                GO
                INSERT [dbo].[Customers] ([CustomerId], [LastName], [FirstName], [BirthDate], [Adress1], [Adress2], [PostCode], [City], [Email], [Phone], [Job], [UserId]) VALUES (N'efbebb36-0bd1-4f62-900d-08d8b0b8fa3b', N'DUPIN', N'Pierre', N'29/09/1983', N'3, rue Eliane', NULL, N'31200', N'TOULOUSE', N'', N'06 59 42 13 25', N'Opticien', N'b58846ed-49b6-4edd-a857-be45753a71c3')
                GO
                INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'cc12905b-0b65-471f-9aa2-f1074dc9b96f', N'admin@email.com', N'ADMIN@EMAIL.COM', N'admin@email.com', N'ADMIN@EMAIL.COM', 1, N'AQAAAAEAACcQAAAAEK2Z+TB7i/EPW8jvE6/ciw+72cK4pEz1vYLV84UuXgnnXQy5c83WVetp5/ytPYsotQ==', N'56FN4GSZWX2UBOHHJLCRLMQELD6NU7NK', N'2e591aa2-5827-4003-a717-e55e4bc0b951', NULL, 0, 0, NULL, 1, 0)
                INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'cc12905b-0b65-471f-9aa2-f1074dc9b96f', N'22781f20-67cb-4e0e-baab-d9c5bc97893d')
                GO
                INSERT [dbo].[Pages] ([PageId], [Name], [Url], [Content]) VALUES (N'a7293247-56cd-4841-f10b-08d8a32f7e11', N'Accueil', N'Accueil', NULL)
                INSERT [dbo].[Pages] ([PageId], [Name], [Url], [Content]) VALUES (N'e8288cb5-9e87-472d-f10c-08d8a32f7e11', N'Coaching', N'Coaching', NULL)
                INSERT [dbo].[Pages] ([PageId], [Name], [Url], [Content]) VALUES (N'9844e840-e355-49bc-f10d-08d8a32f7e11', N'Bootcamp', N'Bootcamp', NULL)
                INSERT [dbo].[Pages] ([PageId], [Name], [Url], [Content]) VALUES (N'deff6979-e4dc-460a-1b52-08d8a361560b', N'Tarifs', N'Tarifs', NULL)
                INSERT [dbo].[Pages] ([PageId], [Name], [Url], [Content]) VALUES (N'deff6979-e4dc-460a-1b52-08d8a361560c', N'Contact', N'Contact', NULL)
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Customers");
        }
    }
}
