﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RGCoaching.Migrations
{
    public partial class AssociatePagesToBlocks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlockPage");

            migrationBuilder.CreateTable(
                name: "PageBlocks",
                columns: table => new
                {
                    PageBlockId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PageId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    BlockId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Order = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PageBlocks", x => x.PageBlockId);
                    table.ForeignKey(
                        name: "FK_PageBlocks_Blocks_BlockId",
                        column: x => x.BlockId,
                        principalTable: "Blocks",
                        principalColumn: "BlockId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PageBlocks_Pages_PageId",
                        column: x => x.PageId,
                        principalTable: "Pages",
                        principalColumn: "PageId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PageBlocks_BlockId",
                table: "PageBlocks",
                column: "BlockId");

            migrationBuilder.CreateIndex(
                name: "IX_PageBlocks_PageId",
                table: "PageBlocks",
                column: "PageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PageBlocks");

            migrationBuilder.CreateTable(
                name: "BlockPage",
                columns: table => new
                {
                    BlocksBlockId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PagesPageId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlockPage", x => new { x.BlocksBlockId, x.PagesPageId });
                    table.ForeignKey(
                        name: "FK_BlockPage_Blocks_BlocksBlockId",
                        column: x => x.BlocksBlockId,
                        principalTable: "Blocks",
                        principalColumn: "BlockId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlockPage_Pages_PagesPageId",
                        column: x => x.PagesPageId,
                        principalTable: "Pages",
                        principalColumn: "PageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlockPage_PagesPageId",
                table: "BlockPage",
                column: "PagesPageId");
        }
    }
}
