﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RGCoaching.Migrations
{
    public partial class PageBlocks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "Pages");

            migrationBuilder.DropColumn(
                name: "Content",
                table: "Pages");

            migrationBuilder.CreateTable(
                name: "Blocks",
                columns: table => new
                {
                    BlockId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Html = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blocks", x => x.BlockId);
                });

            migrationBuilder.CreateTable(
                name: "BlockPage",
                columns: table => new
                {
                    BlocksBlockId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PagesPageId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlockPage", x => new { x.BlocksBlockId, x.PagesPageId });
                    table.ForeignKey(
                        name: "FK_BlockPage_Blocks_BlocksBlockId",
                        column: x => x.BlocksBlockId,
                        principalTable: "Blocks",
                        principalColumn: "BlockId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlockPage_Pages_PagesPageId",
                        column: x => x.PagesPageId,
                        principalTable: "Pages",
                        principalColumn: "PageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlockPage_PagesPageId",
                table: "BlockPage",
                column: "PagesPageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlockPage");

            migrationBuilder.DropTable(
                name: "Blocks");

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Pages",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "Pages",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
