﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RGCoaching.Migrations
{
    public partial class AddNameInPrograms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Programs_Customers_CustomerId",
                table: "Programs");

            migrationBuilder.AlterColumn<Guid>(
                name: "CustomerId",
                table: "Programs",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Programs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Programs_Customers_CustomerId",
                table: "Programs",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Programs_Customers_CustomerId",
                table: "Programs");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Programs");

            migrationBuilder.AlterColumn<Guid>(
                name: "CustomerId",
                table: "Programs",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Programs_Customers_CustomerId",
                table: "Programs",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
