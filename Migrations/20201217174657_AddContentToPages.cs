﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RGCoaching.Migrations
{
    public partial class AddContentToPages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "Pages",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Content",
                table: "Pages");
        }
    }
}
