﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RGCoaching.Models;

namespace RGCoaching.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    [Route("/[area]/[controller]/[action]")]
    [Area("Admin")]
    public class MailBoxController : Controller
    {
        private readonly RGCoachingDbContext _context;

        public MailBoxController(RGCoachingDbContext injectedContext)
        {
            _context = injectedContext;
        }
     
   
        public IActionResult Index()
        {
            return View(_context.Contacts.OrderByDescending(e => e.SendingDay));
        }

        [HttpGet]
        public IActionResult DeleteMail([FromQuery] Guid contactId)
        {
            Contact contact = _context.Contacts.Single(p => p.ContactId == contactId);
            _context.Contacts.Remove(contact);
            _context.SaveChanges();
            return RedirectToAction(controllerName: "MailBox", actionName: "Index");
        }

        [ActionName("GetMailEdit")]
        public IActionResult GetMailEdit([FromQuery] Guid contactId)
        {

            Contact contact = _context.Contacts.Single(p => p.ContactId == contactId);
            contact.EmailLu = false;
            _context.Contacts.Update(contact);
            _context.SaveChanges();

            return View("EditMail", contact);
        }
    }
}
