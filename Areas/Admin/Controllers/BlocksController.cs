﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RGCoaching.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RGCoaching.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    [Route("/[area]/[controller]/[action]")]
    [Area("Admin")]
    public class BlocksController : Controller
    {
        private readonly RGCoachingDbContext _context;

        public BlocksController(RGCoachingDbContext injectedContext)
        {
            _context = injectedContext;
        }

        [HttpGet("/[area]/[controller]/Edit")]
        [ActionName("GetEdit")]
        public ActionResult GetEdit([FromQuery] Guid blockId)
        {
            Block block = _context.Blocks.SingleOrDefault(b => b.BlockId == blockId);
            if (block == null)
            {
                block = new Block();
            }
            return View("Edit", block);
        }

        [HttpPost("/[area]/[controller]/Edit")]
        [ActionName("PostEdit")]
        public ActionResult PostEdit([FromForm] Block block)
        {
            if (_context.Blocks.Any(b => b.BlockId == block.BlockId))
            {
                _context.Entry(block).State = EntityState.Modified;
            }
            else
            {
                _context.Entry(block).State = EntityState.Added;
            }
            _context.SaveChanges();
            return RedirectToAction(controllerName: "Blocks", actionName: "List");
        }

        [HttpGet]
        public ActionResult Delete([FromQuery] Guid blockId)
        {
            Block block = _context.Blocks.SingleOrDefault(b => b.BlockId == blockId);
            _context.Remove(block);
            _context.SaveChanges();
            return RedirectToAction(controllerName: "Blocks", actionName: "List");
        }

        [HttpGet]
        public ActionResult List()
        {
            return View(_context.Blocks);
        }
    }
}
