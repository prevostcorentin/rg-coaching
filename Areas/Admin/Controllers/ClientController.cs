﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RGCoaching.Models;

namespace RGCoaching.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    [Area("Admin")]
    [Route("/[area]/[controller]/[action]")]
    public class ClientController : Controller
    {
        private readonly RGCoachingDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public ClientController(RGCoachingDbContext injectedContext,
                                UserManager<IdentityUser> userManager)
        {
            _context = injectedContext;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult List()
        {
            return View(_context.Customers);
        }

        [HttpPost("/[area]/[controller]/Edit")]
        public IActionResult PostEdit([FromForm] Customer customer)
        {
            IdentityUser user = new IdentityUser
            {
                Email = customer.Email,
                UserName = customer.Email
            };
            _userManager.CreateAsync(user).Wait();
            _context.Update(customer);
            _context.SaveChanges();
            return View("List", _context.Customers);
        }

        [HttpGet("/[area]/[controller]/Edit")]
        public IActionResult GetEdit([FromQuery] Guid customerId)
        {
            Customer customer = _context.Customers.SingleOrDefault(customer => customer.CustomerId == customerId);
            if (customer == null)
            {
                customer = new Customer();
            }
            return View("Edit", customer);
        }

        [HttpGet]
        public IActionResult Delete([FromQuery] Guid customerId)
        {
            Customer customer = _context.Customers.Single(c => c.CustomerId == customerId);
            _context.Customers.Remove(customer);
            _context.SaveChanges();
            return View("List", _context.Customers);
        }
    }
}
