﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RGCoaching.Models;

namespace RGCoaching.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    [Route("/[area]/[controller]/[action]")]
    [Area("Admin")]
    public class PagesController : Controller
    {
        private readonly RGCoachingDbContext _context;

        public PagesController(RGCoachingDbContext injectedContext)
        {
            _context = injectedContext;
        }

        [HttpGet("/[area]/[controller]/Edit")]
        [ActionName("GetEdit")]
        public IActionResult GetEdit([FromQuery] Guid pageId)
        {
            Page page = _context.Pages.SingleOrDefault(p => p.PageId == pageId); 
            if (page == null)
            {
                page = new Page();
            }
            if (_context.PageBlocks != null)
            {
                page.PageBlocks = _context.PageBlocks.Include(b => b.Block)
                                                     .Where(pb => pb.Page.PageId == pageId)
                                                     .ToList();
            }
            return View("Edit", page);
        }

        [HttpPost("/[area]/[controller]/Edit")]
        [ActionName("PostEdit")]
        public IActionResult PostEdit([FromForm] Page page)
        {
            if (_context.Pages.Any(p => p.PageId == page.PageId))
            {
                _context.Entry(page).State = EntityState.Modified;
            }
            else
            {
                _context.Entry(page).State = EntityState.Added;
            }
            _context.SaveChanges();
            return RedirectToAction(controllerName: "Pages", actionName:"List");
        }

        [HttpGet]
        public IActionResult Delete([FromQuery] Guid pageId)
        {
            Page page = _context.Pages.Single(p => p.PageId == pageId);
            _context.Pages.Remove(page);
            _context.SaveChanges();
            return RedirectToAction(controllerName: "Pages", actionName:"List");
        }

        [HttpGet]
        public IActionResult List()
        {
            return View(_context.Pages);
        }

    }
}
