﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RGCoaching.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RGCoaching.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    [Area("Admin")]
    [Route("/[area]/[controller]/[action]")]
    public class PageBlocksController : Controller
    {
        private readonly RGCoachingDbContext _context;

        public PageBlocksController(RGCoachingDbContext context)
        {
            _context = context;
        }

        [HttpGet("/[area]/[controller]/Edit")]
        public IActionResult GetEdit([FromQuery] Guid pageBlockId, [FromQuery] Guid blockId)
        {
            Block block = _context.Blocks.SingleOrDefault(b => b.BlockId == blockId);
            if (block == null)
            {
                block = new Block();
            }
            PageBlock pageBlock = _context.PageBlocks.SingleOrDefault(pb => pb.PageBlockId == pageBlockId);
            if (pageBlock == null)
            {
                pageBlock = new PageBlock();
            }
            pageBlock.Block = block;
            return View("Edit", pageBlock);
        }

        [HttpPost("/[area]/[controller]/Edit")]
        public IActionResult PostEdit([FromForm] PageBlock pageBlock)
        {
            pageBlock.Page = _context.Pages.SingleOrDefault(p => p.PageId == pageBlock.Page.PageId);
            pageBlock.Block = _context.Blocks.SingleOrDefault(b => b.BlockId == pageBlock.Block.BlockId);
           _context.Update(pageBlock);
            _context.SaveChanges();
            return RedirectToAction(controllerName: "Blocks", actionName: "GetEdit", 
                                    routeValues: new { blockId = pageBlock.Block.BlockId });
        }

        [HttpGet]
        public IActionResult Delete([FromQuery] Guid pageBlockId)
        {
            PageBlock pageBlock = _context.PageBlocks.SingleOrDefault(pb => pb.PageBlockId == pageBlockId);
            _context.Remove(pageBlock);
            _context.SaveChanges();
            return RedirectToAction(controllerName: "Blocks", actionName: "List");
        }
    }
}
