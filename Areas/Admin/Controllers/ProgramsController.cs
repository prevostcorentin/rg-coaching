﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RGCoaching.Models;
using RGCoaching.ViewModels;


namespace RGCoaching.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    [Route("/[area]/[controller]/[action]")]
    [Area("Admin")]
    public class ProgramsController : Controller
    {
        private readonly RGCoachingDbContext _context;
        private readonly IWebHostEnvironment hostingEnvironment;

        public ProgramsController(RGCoachingDbContext injectedContext, IWebHostEnvironment hostingEnvironment)
        {
            _context = injectedContext;
            this.hostingEnvironment = hostingEnvironment;
        }
        //Route pour ajouter un nouveau programme - Utilisée au submit du formulaire affiché par le GetEdit
        [HttpPost("/[area]/[controller]/Edit")]
        [ActionName("PostEdit")]
        public IActionResult PostEdit([FromForm] ProgramCreateViewModel model)
        {
            string uniqueFileName = null;
            string uniqueFileName2 = null;
            string uniqueFileName3 = null;
            Customer customer = _context.Customers.Single(c => c.CustomerId == model.Customer.CustomerId);
            
            if (model.Document != null)
            {
                string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "documents");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Document.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                model.Document.CopyTo(new FileStream(filePath, FileMode.Create));
            }

            if (model.Document2 != null)
            {
                string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "documents");
                uniqueFileName2 = Guid.NewGuid().ToString() + "_" + model.Document2.FileName;
                string filePath2 = Path.Combine(uploadsFolder, uniqueFileName2);
                model.Document2.CopyTo(new FileStream(filePath2, FileMode.Create));
            }

            if (model.Document3 != null)
            {
                string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "documents");
                uniqueFileName3 = Guid.NewGuid().ToString() + "_" + model.Document3.FileName;
                string filePath3 = Path.Combine(uploadsFolder, uniqueFileName3);
                model.Document3.CopyTo(new FileStream(filePath3, FileMode.Create));
            }

            Models.Program newProgram = new Models.Program
            {
                Cycle = model.Cycle,
                BeginDate = model.BeginDate,
                EndDate = model.EndDate,
                Name = model.Name,
                DocumentPath = uniqueFileName,
                Name2 = model.Name2,
                DocumentPath2 = uniqueFileName2,
                Name3 = model.Name3,
                DocumentPath3 = uniqueFileName3,
                Customer = customer
            };

            _context.Add(newProgram);
            _context.SaveChanges();
            return RedirectToAction(controllerName: "Programs", actionName: "List");
        }

        //Route pour afficher le formulaire pour ajouter un nouveau programme - Utilisée dans le layout Admin
        [HttpGet("/[area]/[controller]/Edit")]
        public IActionResult GetEdit()
        {
            return View("Edit", new ViewModels.ProgramCreateViewModel());
        }      

        //Route pour supprimer un programme - Utilisée dans la liste des programmes sur le bouton Poubelle
        [HttpGet]
        public IActionResult Delete([FromQuery] Guid programId)
        {
            Models.Program program = _context.Programs.Single(p => p.ProgramId == programId);
            _context.Programs.Remove(program);
            _context.SaveChanges();
            return RedirectToAction(controllerName: "Programs", actionName: "List");
        }

        //Route pour afficher la liste de tous les programmes - Utilisée dans le layout Admin
        [HttpGet]
        public IActionResult List()
        {
            return View(_context.Programs.Include(p => p.Customer));
        }
    }
}