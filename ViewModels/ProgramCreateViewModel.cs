﻿using Microsoft.AspNetCore.Http;
using RGCoaching.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RGCoaching.ViewModels
{
    public class ProgramCreateViewModel
    {
        public Guid ProgramId { get; set; }
            
        [Required]
        public String Cycle { get; set; }

        [Required]
        public String BeginDate { get; set; }

        [Required]
        public String EndDate { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        public IFormFile Document { get; set; }

        public String Name2 { get; set; }
        public IFormFile Document2 { get; set; }
        public String Name3 { get; set; }
        public IFormFile Document3 { get; set; }

        [Required]
        public Customer Customer { get; set; }
    }
}
