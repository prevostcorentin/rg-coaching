﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RGCoaching.Models
{
    public class Contact
    {
        public Guid ContactId { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public String Object { get; set; }
        public String Text { get; set; }
        public DateTime SendingDay { get; set; }
        public bool EmailLu { get; set; }
    }
}
