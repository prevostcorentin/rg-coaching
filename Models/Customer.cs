﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RGCoaching.Models
{
	public class Customer
	{
		public Guid CustomerId { get; set; }
		public String Gender { get; set; }
		public String LastName { get; set; }
		public String FirstName { get; set; }
		public String BirthDate { get; set; }
		public String Adress1 { get; set; }
		public String Adress2 { get; set; }
		public String PostCode { get; set; }
		public String City { get; set; }
		public String Email { get; set; }
		public String Phone { get; set; }
		public String Job { get; set; }
		public ICollection<Models.Program> Programs { get; set; }

		public IdentityUser User {get; set;} 
	}
}
