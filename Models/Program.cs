﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RGCoaching.Models
{
    public class Program
    {
        public Guid ProgramId { get; set; }
        public String Cycle { get; set; }
        public String BeginDate { get; set; }
        public String EndDate { get; set; }
        public String Name { get; set; }
        public String DocumentPath { get; set; }
        public String Name2 { get; set; }
        public String DocumentPath2 { get; set; }
        public String Name3 { get; set; }
        public String DocumentPath3 { get; set; }

        [Required]
        public Customer Customer { get; set; }
    }
}
