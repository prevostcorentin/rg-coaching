﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RGCoaching.Models
{
    public class MenuLink
    {
        public Guid MenuLinkId { get; set; }
        public Page Page { get; set; }
        public String Text { get; set; }
    }
}
