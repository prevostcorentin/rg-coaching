﻿using System;
using System.Collections.Generic;

namespace RGCoaching.Models
{
    public class Block
    {
        public Guid BlockId { get; set; }
        public String Name { get; set; }
        public String Html { get; set; }
        public ICollection<PageBlock> PageBlocks { get; set; }
    }
}