﻿using System;

namespace RGCoaching.Models
{
    public class PageBlock
    {
        public Guid PageBlockId { get; set; }
        public Page Page { get; set; }
        public Block Block { get; set; }
        public Int32 Order { get; set; }
    }
}
