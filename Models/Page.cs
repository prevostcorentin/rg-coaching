﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RGCoaching.Models
{
    public class Page
    {
        public Guid PageId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public ICollection<PageBlock> PageBlocks { get; set; }
    }
}
