﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using RGCoaching.Data;
using RGCoaching.Models;

namespace RGCoaching.Controllers
{
    public class PagesController : Controller
    {
        private readonly RGCoachingDbContext _context;
       
        public PagesController(ILogger<PagesController> logger,
                              RGCoachingDbContext context)
        {
            _context = context;
        }

        [ActionName("ServePage")]
        [HttpGet("/[controller]/{*url}")]
        [HttpGet("/{url=index}")]
        public IActionResult ServePage(String url)
        {
            Models.Page page = _context.Pages.SingleOrDefault(
                p => p.Url.ToLower() == url.ToLower()
            );
            if (page == null)
            {
                return NotFound();
            }
            page.PageBlocks = _context.PageBlocks.Include(pb => pb.Block)
                                                 .Where(pb => pb.Page.PageId == page.PageId)
                                                 .ToList();
            return View(page);
        }
    }
}
