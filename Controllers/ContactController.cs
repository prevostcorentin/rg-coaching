﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RGCoaching.Models;

namespace RGCoaching.Areas.User.Controllers
{
    public class ContactController : Controller
    {
        private readonly RGCoachingDbContext _context;

        public ContactController(RGCoachingDbContext context)
        {
            _context = context;
        }

        [Route("/Pages/Contact")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("/Pages/Send")]
        public IActionResult Send([FromForm] Contact content)
        {
            _context.Add(content);
            _context.SaveChanges();
            return View();
        }
    }
}
