﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace RGCoaching.Controllers
{
    public class RoleController : Controller
    {
        private RoleManager<IdentityRole> _roleManager;

        public RoleController(RoleManager<IdentityRole> injectedRoleManager)
        {
            _roleManager = injectedRoleManager;
        }

        public IActionResult Add(String roleName)
        {
            IdentityRole role = new IdentityRole(roleName);
            var result = _roleManager.CreateAsync(role);
            if (result.Result.Succeeded)
            {
                return StatusCode(200);
            }
            else
            {
                return Json(result.Result.Errors);
            }
        }
    }
}
