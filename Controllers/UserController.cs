﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RGCoaching.Models;

namespace RGCoaching.Areas.User.Controllers
{
    [Authorize(Roles = "User")]
    [Route("/[controller]/[action]")]
    public class UserController : Controller
    {
        private readonly RGCoachingDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public UserController(RGCoachingDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
            
        public IActionResult Index()
        {
            String userId = _userManager.GetUserId(User);
            Customer customer = _context.Customers.Include(customer => customer.Programs)
                                                  .Include(customer => customer.User)
                                                  .Single(c => c.User.Id == userId);
            return View(customer);
        }

        [HttpPost]
        public IActionResult Update([FromForm] Customer customer)
        {
            _context.Entry(customer).State = EntityState.Modified;
            _context.SaveChanges();
            return RedirectToAction(actionName: "Index", controllerName: "User");
        }
    }
}
